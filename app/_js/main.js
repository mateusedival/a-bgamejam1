var probabilidade = 20;
var initTime = 0;
var atualizouDif = 0;
var snakeVel = 80;
var stoneVel = 50;
var gameover = false;
var scoreText;
var timeScore = 0;
var player;
var snakes;
var stones;
var cursors;
var flag = false;

var menu = new Audio('app/sounds/menu.mp3');
var ingame = new Audio('app/sounds/ingame.mp3');
var gover = new Audio('app/sounds/gameover.mp3');

menu.play();

function GeraObstaculos()
{
    if((Math.floor(Math.random() * 100 + 1) <= probabilidade ) && !gameover)
    {
        if(Math.floor(Math.random() * 100 + 1) > 22)
        {
            var snake = snakes.create(Math.floor(Math.random() * 500 + 50),20, 'snake');
            snake.setVelocityY(snakeVel);
        }
        else
        {
            var stone = stones.create(Math.floor(Math.random() * 500 + 50),20, 'stone');
            stone.setVelocityY(stoneVel);
        }
    }
}

function hitSnake()
{
    game.loop.pause();
    this.physics.pause();
    gameover = true;
    this.scene.stop('jogo');
    this.scene.start('EndGame');
}


class jogo extends Phaser.Scene {
    constructor ()
    {
        super({ key: 'jogo' });

        setInterval(function(){GeraObstaculos()}, 1000);
        setInterval(function() { initTime++; }, 1000);
    }
    preload()
    {
        this.load.image('fundo', 'app/images/areia.png');
        this.load.spritesheet('player','app/images/dude.png',{ frameWidth: 32, frameHeight: 48 }); //teste de como usar sprites, sprite padrão do Phaser
        this.load.image('ll', 'app/images/lagarto-idle-frame-1.png');
        this.load.spritesheet('calango','app/images/calango.png',{ frameWidth: 54.5, frameHeight: 64});
        this.load.spritesheet('snake', 'app/images/cobra.gif',{ frameWidth: 70, frameHeight: 70});
        this.load.image('stone', 'app/images/pedra.png');
    }

    create()
    {
        probabilidade = 20;
        initTime = 0;
        atualizouDif = 0;
        snakeVel = 80;
        stoneVel = 50;
        gameover = false;
        timeScore = 0;

        gover.pause();
        this.add.image(300, 400, 'fundo');

        snakes = this.physics.add.group();
        stones = this.physics.add.group();

        player = this.physics.add.sprite(300, 680, 'calango');
        player.setBounce(0.2);
        player.setCollideWorldBounds(true);

        scoreText = this.add.text(16, 16, 'mts: 0', { fontSize: '32px', fill: '#000' });

        //animação do player
        if(!flag)
        {
            this.anims.create({
                key: 'left',
                frames: this.anims.generateFrameNumbers('calango', { start: 0, end: 3 }),
                frameRate: 3,
                repeat: -1
            });
            this.anims.create({
                key: 'right',
                frames: this.anims.generateFrameNumbers('calango', { start: 4, end: 7 }),
                frameRate: 3,
                repeat: -1
            });
            flag = true;
        }
        cursors = this.input.keyboard.createCursorKeys();

        this.physics.add.collider(player, snakes, hitSnake, null, this);
        this.physics.add.collider(player, stones, hitSnake, null, this);

        player.anims.play('left', true);
        player.anims.play('turn', true);

        player.setVelocityY(0);
        player.setVelocityX(0);
    }

    update()
    {
        if(gameover)
        {
            return;
        }
        ingame.play();

        player.setVelocityY(0);
        player.setVelocityX(0);

        if(cursors.left.isDown && cursors.up.isDown)
        {
            player.setVelocityX(-100);
            player.setVelocityY(-75);
        }
        else if(cursors.left.isDown && cursors.down.isDown)
        {
            player.setVelocityX(-100);
            player.setVelocityY(75);
        }
        else if(cursors.right.isDown && cursors.up.isDown)
        {
            player.setVelocityX(100);
            player.setVelocityY(-75);
        }
        else if(cursors.right.isDown && cursors.down.isDown)
        {
            player.setVelocityX(100);
            player.setVelocityY(75);
        }
        else if(cursors.left.isDown)
        {
            player.setVelocityX(-200);
            player.anims.play('left', true);
        }
        else if(cursors.right.isDown)
        {
            player.setVelocityX(200);
            player.anims.play('right', true);
        }
        if(cursors.up.isDown)
        {
            player.setVelocityY(-150);
        }
        else if(cursors.down.isDown)
        {
            player.setVelocityY(150);
        }
        if(initTime >= atualizouDif+10)
        {
            probabilidade += 5;
            snakeVel += 5;
            stoneVel += 5;
            atualizouDif = initTime;
        }
        if(initTime >= timeScore+2)
        {
            timeScore++;
            scoreText.setText('mts: ' + timeScore);
        }
    }
}
class Menu extends Phaser.Scene {

    constructor ()
    {
        super({ key: 'Menu' });
    }

    preload ()
    {
        this.load.image('fundo', 'app/images/areia.png');
        this.load.image('logo', 'app/images/logo.png');
        this.load.image('play', 'app/images/play.png');

        this.load.audio('fundo', 'app/sounds/sound.mp3', {
            instances: 1
        });
    }

    create ()
    {
        this.sound.add('fundo');
        this.add.image(300, 400, 'fundo');
        this.add.image(300, 150, 'logo');

        this.face = this.add.image(300, 500, 'play');

        this.input.manager.enabled = true;

        this.input.once('pointerdown', function ()
        {
            menu.pause();
            this.scene.start('jogo');
        }, this);
    }
}

class EndGame extends Phaser.Scene {

    constructor ()
    {
        super({ key: 'EndGame' });
    }

    preload ()
    {
        this.load.image('fundo', 'app/images/areia.png');
        this.load.image('cascavel', 'app/images/game-over.png');
        this.load.image('reload', 'app/images/reload.png');
    }

    create ()
    {
        this.add.image(300, 400, 'fundo');
        this.add.image(300, 350, 'cascavel');
        scoreText = this.add.text(20, 400, 'GAME OVER', { fontSize: '100px', fill: 'red' });

        this.face = this.add.image(300, 600, 'reload');

        this.input.manager.enabled = true;

        ingame.pause();
        gover.play();


        this.input.once('pointerdown', function () {
            this.scene.stop('jogo');
            this.scene.start('jogo');
        }, this);
    }
}
var config = {
        type: Phaser.CANVAS,
        width: 600,
        height: 750,
        physics: {
            default: 'arcade',
            arcade: {
                gravity: { y: 0 },
                debug: false
            }
        },
        scene: [Menu,jogo,EndGame]
    };

var game = new Phaser.Game(config);
